import { useEffect, useState } from "react";

// Used to check current position for components display
const useScrollPosition = (): number => {
  const [position, setPosition] = useState(0);

  const handleScroll = (): void => {
    // Returns nothing as it sets state
    const pagePosition: number = window.pageYOffset;
    setPosition(pagePosition);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    // Cleanup function
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return position;
};

export default useScrollPosition;
