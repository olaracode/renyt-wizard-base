import { extendTheme } from "@chakra-ui/react";
import themeComponents from "./themeComponents";
import themeFonts from "./themeFonts";

// Theme Creation
export const darkTheme = extendTheme({
  styles: {
    global: {
      body: {
        maxW: "100vw",
        overflowX: "hidden",
        color: "brand.white",
        backgroundColor: "brand.dark",
      },
    },
  },
  colors: {
    brand: {
      primary: "#f24b5e",
      secondary: "#f37c83",
      dark: "#0d102d",
      light: "#dfe1f5",
      white: "#fafafa",
      100: "#DDDDDD",
    },
  },
  fonts: {
    ...themeFonts,
  },
  components: {
    ...themeComponents,
  },
});
