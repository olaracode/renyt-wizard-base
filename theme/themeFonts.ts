const themeFonts = {
  heading: `"CalSans", sans-serif`,
  body: `"CalSans", sans-serif`,
};

export default themeFonts;
