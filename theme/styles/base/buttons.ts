import { defineStyleConfig } from "@chakra-ui/react";
const Button = defineStyleConfig({
  baseStyle: {
    backgroundColor: "brand.primary",
    borderRadius: "6px",
    fontWeight: "normal",
    color: "brand.white",
  },
  variants: {
    "nav-button": {
      backgroundColor: "brand.transparent",
      _hover: { backgroundColor: "brand.primary" },
      ariaLabel: "",
      borderRadius: "100px",
    },
    "cta-button": {
      w: { base: "100px", md: "180px" },
      fontWeight: "bold",
    },
    primary: {},
    outline: {
      backgroundColor: "none",
      color: "brand.accent",
      borderColor: "brand.accent",
      _hover: {
        backgroundColor: "brand.accentLight",
      },
      _active: {
        backgroundColor: "transparent",
      },
      _disabled: {
        filter: "brightness(60%)",
        cursor: "default",
      },
    },
    ghost: {
      backgroundColor: "none",
      _hover: {
        backgroundColor: "brand.primary",
      },
    },
  },
  defaultProps: {
    variant: "primary",
  },
});

export default Button;
