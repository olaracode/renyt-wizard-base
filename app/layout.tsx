"use client";
import { ChakraProvider } from "@chakra-ui/react";
import { darkTheme } from "../theme/theme";
import Navbar from "./components/Navbar";
export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html>
      <head />
      <body>
        <ChakraProvider theme={darkTheme}>
          <Navbar />
          {children}
        </ChakraProvider>
      </body>
    </html>
  );
}
