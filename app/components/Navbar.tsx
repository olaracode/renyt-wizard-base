import React, { FC } from "react";
import { Box } from "@chakra-ui/react";
const Navbar: FC = () => {
  return (
    <Box p="5" borderBottom={"1px"} borderColor="brand.primary">
      Navbar
    </Box>
  );
};

export default Navbar;
