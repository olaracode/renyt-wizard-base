import React from "react";

const Template = ({ children }: { children: React.ReactNode }) => {
  /**
   * Aqui podriamos usar la ruta actual para definir, el endpoint al que consulta,
   * usando una constante tipo array de objetos, con el correspondiente endpoint,
   * a los children se les puede pasar valores?
   *
   * Tambien con la ruta actual podriamos definir la navegacion, usando currentRoute + 1 con useRouter o link
   */
  return (
    <div>
      Boton de accion
      {children}
      Navigation
    </div>
  );
};

export default Template;
