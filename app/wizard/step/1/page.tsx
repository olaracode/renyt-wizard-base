import React from "react";

const page = () => {
  /**
   * Este ya seria el form per se
   *
   * Como vamos a manejar los inputs? con useRef() para evitar re renders
   * react-hook-forms, o vieja escuela con useState?
   */
  return <div>Step 1</div>;
};

export default page;
