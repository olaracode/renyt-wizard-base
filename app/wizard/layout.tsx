"use client";
import React, { ReactNode } from "react";
import { SimpleGrid, GridItem, Box } from "@chakra-ui/react";
type PropsT = {
  children: ReactNode;
};
const layout = ({ children }: PropsT) => {
  /**
   * Aqui podemos usar la misma logica de detectar cambios
   * en la ruta para volver a fetchear la informacion del progreso del wizard
   */
  return (
    <Box px="10vh">
      <SimpleGrid columns={12}>
        <GridItem colSpan={{ base: 12, md: 4, lg: 2 }} border="1px solid red">
          Sidebar
        </GridItem>
        <GridItem colSpan={{ base: 12, md: 8, lg: 10 }} border="1px solid red">
          {children}
        </GridItem>
      </SimpleGrid>
    </Box>
  );
};

export default layout;
